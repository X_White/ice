#include "stdafx.h"
#include "Dao.h"

#include <cstring>

namespace DaoOp {
	Dao::Dao() {}
	Dao::Dao(string userName, string password) {
		this->userName = userName;
		this->password = password;
	}

	Dao::~Dao() {}

	bool Dao::init() {
		if(false == initKey()) return false;
		if(false == initFile()) return false;

//		mysql_host = decode(mysql_host);
//		mysql_name = decode(mysql_name);
//		mysql_pwd = decode(mysql_pwd);
		
		if(false == initSQL()) return false;
		return true;
	}

	bool Dao::initKey() {
		key = "wTheisgksdJKDASkfd";
		return true;
	}

	bool Dao::initFile() {
		FILE* fp;
		char linex[1024];
		errno_t err;

		err = freopen_s(&fp, "./conf/info.conf", "r", stdin);

		if(err != 0) {
			lastError = new string("Can't open file: ./conf/info.conf\nPlease check");
			return false;
		}

		while(!feof(fp)) {
			fgets(linex, 1024, fp);
			if('#' == linex[0]) continue;
			char n1[128], n2[128], equ[128];
			memset(n1, 0, sizeof(n1));
			memset(n2, 0, sizeof(n2));
			memset(equ, 0, sizeof(equ));
			sscanf_s(linex, "%s%s%s", n1, sizeof(n1), equ, sizeof(equ), n2, sizeof(n2));
				
			if(!strcmp(n1, "DataBase")) {
				dbname = n2;
			}
			else if(!strcmp(n1, "Table")) {
				tableName = n2;
			}
			else if(!strcmp(n1, "HostName")) {
				mysql_host = n2;
			}
			else if(!strcmp(n1, "SqlName")) {
				mysql_name = n2;
			}
			else if(!strcmp(n1, "SqlPwd")) {
				mysql_pwd = n2;
			}
		}
		return true;
	}

	bool Dao::initSQL() {
		conn = mysql_init(NULL);
		if(!conn) {
			lastError = new string("mysql_init failed: %s", mysql_errno(conn));
			return false;
		}
		conn = mysql_real_connect(conn, mysql_host.c_str(), mysql_name.c_str(), mysql_pwd.c_str(), dbname.c_str(), 0, NULL, 0);
		if(!conn) {
			lastError = new string("mysql_real_connect failed\n\tConnection error: %s", mysql_error(conn));
			return false;
		}
		return true;
	}

	string Dao::encode(string in) {
		int len = in.length();
		string retval;
		for(int i = 0; i < len; i++) {
			retval[i] = in[i] - (key[i] - 'A');
		}
		return retval;
	}

	string Dao::decode(string in) {
		int len = in.length();
		string retval;
		for(int i = 0; i < len; i++) {
			retval[i] = in[i] + (key[i] - 'A');
		}
		return retval;
	}

	void Dao::setUserName(string userName) {
		this->userName = userName;
	}

	string Dao::getUserName() {
		return userName;
	}

	void Dao::setPassword(string password) {
		this->password = password;
	}

	void Dao::setKey(string key) {
		this->key = key;
	}

	string Dao::getKey() {
		return key;
	}

	string* Dao::getLastError() {
		return lastError;
	}

}

