#include <iostream>
#include <cstring>
#include <mysql.h>

using namespace std;

namespace DaoOp {
	class Dao {
	public:
		Dao();
		Dao(string userName, string password);

		~Dao();

		bool init();
		bool initKey();
		bool initFile();
		bool initSQL();
		string encode(string in);
		string decode(string in);
		void setUserName(string userName);
		string getUserName();
		void setPassword(string password);
		void setKey(string key);
		string getKey();

		string* getLastError();

	private:
		MYSQL* conn;
		string mysql_host, mysql_name, mysql_pwd, dbname, tableName;
		string userName, password;	
		string key;
		string *lastError;

	};
};