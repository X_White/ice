// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

#include <Ice/Ice.h>
#include <Printer.h>
#include <DBOperator.h>

#include <cstdio>
#include <iostream>
#include <cstring>
#include <time.h>
#include <WinSock2.h>
#include <mysql.h>
#include <vector>

#pragma comment(lib, "libmysql.lib")

using namespace std;
using namespace Demo;
using namespace dbOperator;

const int logLen = 22;
string key;

/*
 * Author：X_White
 * 数据库参数格式
*/
typedef struct synData {
	int count;
	char data[20][20];
}synData;

void ice_strcpy(char *src, char *des) {
	size_t i;
	size_t len1 = strlen(src);
	size_t len2 = strlen(des);
	for(i = 0; i < len2; i++) {
		src[i] = des[i];
	}
	src[i] = '\0';
}
/*
 * Author: X_White
 * 将数据库表src转换到des
*/
void transform(char *src, char *tableName, char *des) {

}

/*
 * Author：X_White
 * 数据加密，简单Demo
*/
string encode(string in) {
	string out;
	size_t i;
	size_t len = in.length();
	for(i = 0; i < len; i++) {
		out[i] = in[i] + (key[i] - 'A');
	}
	out[i] = '\0';
	return out;
}
/*
 * Author：X_White
 * 数据解密，简单Demo
*/
string decode(string in) {
	string out;
	size_t i;
	size_t len = in.length();
	for(i = 0;i < len; i++) {
		out[i] = in[i] - (key[i] - 'A');
	}
	out[i] = '\0';
	return out;
}

/*
 * 写入日志文件，目前为空
*/
int writeLog() {
	return 0;
}

/*
 * Author：X_White
 * initConf读取配置文件info.conf 并返回serverIP、端口号、数据库dbName、表名
 *
*/
int initConf(
	char *desIP, char *desPort, char *dbName, char *tableName, char *hostName, char *sqlName, char *sqlPwd,
	char *logBin, char *logErr, char *logLog, char *logSlowQueries
	) {
	FILE *conffp;
	errno_t err;
	char buf[1024];
	err = freopen_s(&conffp, "../conf/info.conf", "r", stdin);
	if(err != 0) {
		writeLog();
		exit(1);
	}
	while(gets_s(buf)) {
		if('#' == buf[0]) continue;
		char n1[128], n2[128], equ[128];
		memset(n1, 0, sizeof(n1));
		memset(n2, 0, sizeof(n2));
		sscanf_s(buf, "%s %s %s", n1, sizeof(n1), equ, sizeof(equ) , n2, sizeof(n2));
//		printf("%s %s\n", tmp, num);
		if(!strcmp(n1, "ServerIP")) {
			ice_strcpy(desIP, n2);
		}
		else if(!strcmp(n1, "ServerPort")) {
			ice_strcpy(desPort, n2);
		}
		else if(!strcmp(n1, "DataBase")) {
			ice_strcpy(dbName, n2);
		}
		else if(!strcmp(n1, "Table")) {
			ice_strcpy(tableName, n2);
		}
		else if(!strcmp(n1, "HostName")) {
			ice_strcpy(hostName, n2);
		}
		else if(!strcmp(n1, "SqlName")) {
			ice_strcpy(sqlName, n2);
		}
		else if(!strcmp(n1, "SqlPwd")) {
			ice_strcpy(sqlPwd, n2);
		}
		else if(!strcmp(n1, "KEY")) {
			key = n2;
		}
		else if(!strcmp(n1, "log-bin")) {
			ice_strcpy(logBin, n2);
		}
		else if(!strcmp(n1, "log-err")) {
			ice_strcpy(logErr, n2);
		}
		else if(!strcmp(n1, "log")) {
			ice_strcpy(logLog, n2);
		}
		else if(!strcmp(n1, "log-slow-queries")) {
			ice_strcpy(logSlowQueries, n2);
		}
	}
	return 0;
}

/*
 * Author：X_White
 * sql_init访问mysql，并取出tableName下所有数据
 *
*/
vector<synData> sql_init(char *dbName, char *tableName, char *hostName, char *sqlName, char *sqlPwd) {
	MYSQL *conn = NULL;
	MYSQL_RES *res;
	MYSQL_ROW row;
	vector<synData> vRet;
	char sql[256];
	
	conn = mysql_init(NULL);
	if(!conn) {
		fprintf(stderr, "mysql_init failed\n");
		exit(1);
	}
	conn = mysql_real_connect(conn, hostName, sqlName, sqlPwd, dbName, 0, NULL, 0);
	if(!conn) {
		fprintf(stderr, "mysql_real_connect failed\n");
		exit(1);
	}
	memset(sql, 0, sizeof(sql));
	sprintf_s(sql, "SELECT * FROM %s", tableName);
	if(mysql_query(conn, sql)) {
		fprintf(stderr, "mysql_query failed\n");
		exit(1);
	}
	if(!(res = mysql_store_result(conn))) {
		fprintf(stderr, "Could not get result\n");
		exit(1);
	}

	while(row = mysql_fetch_row(res)) {
		unsigned int count = mysql_field_count(conn);
		unsigned int index = 0;
		struct synData synInfo;
		synInfo.count = count;
		while(index < count) {
//			printf("%s ", row[index]);
			ice_strcpy(synInfo.data[index], row[index]);
			index++;
		}
		vRet.push_back(synInfo);
//		printf("\n");
	}
	mysql_free_result(res);
	mysql_close(conn);
	return vRet;
}


/*
 * Author: X_White
 * main程序
*/

int main(int argc, char * argv[]) {
    int status = 0;
	Ice::CommunicatorPtr ic;

	char serverIP[128], serverPort[128];
	char dbName[128], tableName[128], hostName[128], sqlName[128], sqlPwd[128];
	char logBin[128], logErr[128], logLog[128], logSlowQueries[128];
	char iceProxyString[128] = "SimplePrinter:default -h ";
	char iceProxyDB[128] = "DBOperator:default -h ";
	vector<synData> iceSynData;

	initConf(serverIP, serverPort, dbName, tableName, hostName, sqlName, sqlPwd, logBin, logErr, logLog, logSlowQueries);
/*
 * strcat_s进行iceProxyString和iceProxyDB的拼接，最后生成
 *		SimplePrinter:default -h 127.0.0.1 -p 10000		此为打印服务
 *		DBOperator:default -h 127.0.0.1 -p 10000		此为数据库服务
 * 用于和Server端链接
*/	
	strcat_s(iceProxyString, serverIP);
	strcat_s(iceProxyString, " -p ");
	strcat_s(iceProxyString, serverPort);

	strcat_s(iceProxyDB, serverIP);
	strcat_s(iceProxyDB, " -p ");
	strcat_s(iceProxyDB, serverPort);
//	printf("%s\n", iceProxyString);

/*
 * 数据库的初始化，并dbName.tableName中取出所有数据，存储在iceSynData
*/
	iceSynData = sql_init(dbName, tableName, hostName, sqlName, sqlPwd);

    try {
// 初始化Ice run time
        ic = Ice::initialize(argc, argv);
// 获取远程代理
        Ice::ObjectPrx base = ic->stringToProxy(iceProxyString);
		Ice::ObjectPrx dbBase = ic->stringToProxy(iceProxyDB);
// 向下转换，将Object接口转为Print和DBOprtr接口
        PrinterPrx printer = PrinterPrx::checkedCast(base);
		DBOprtrPrx dbOpAgent = DBOprtrPrx::checkedCast(dbBase);
        if(!printer) {
            throw "Invalid proxy printer";
        }
		if(!dbOpAgent) {
			throw "Invalid proxy dbOpAgent";
		}
//      printer->printString("Hello World!");
/*
 * Author: X_White
 * 远程代理操作数据库
*/
		if(1 == argc) {			
			int i, j;
			i = 0;
// 首次启动该Client时，进行全部同步，先清空数据表
			dbOpAgent->synDropTab();
// 将Client端从dbName.tableName中获取的数据，即iceSynData进行传送到Server
			for(vector<synData>::iterator it = iceSynData.begin(); it != iceSynData.end(); it++, i++) {
				for(j = 0; j < iceSynData[0].count; j++) {
					char tmp[256];
					sprintf_s(tmp, "%d", j);
					string jIndex(tmp);
// 传送iceSynData，参数为实际数据以及索引
					dbOpAgent->transmitData(iceSynData[i].data[j], jIndex);
				}
// 传送完成后进行逐条插入数据库
				while(dbOpAgent->synRowData() != 0) {
					fprintf(stderr, "synRowData error!\n");				// 此处有待设置超时，以及错误处理
					fprintf(stderr, "Please check\n");
				}
			}
// 进入日志查询阶段
			DWORD hasRead = 0;
			HANDLE hLog;
			hLog = CreateFile(TEXT(logLog), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			hasRead = GetFileSize(hLog, NULL);
			CloseHandle(hLog);
			printf("hasRead: %d\n", hasRead);
			while(1) {				
				DWORD bytesRead, fileSize, i;
				char *bufRead;
				char singleLine[128];
				int si;
				memset(singleLine, 0, sizeof(singleLine));
// 以文件共享、读写锁的方式打开
				hLog = CreateFile(TEXT(logLog), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//				hLog = CreateFile(TEXT(logLog), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_FLAG_DELETE_ON_CLOSE, NULL);
				if(INVALID_HANDLE_VALUE == hLog) {
					fprintf(stderr, "ERROR: Can't open log file: %s\n", logLog);
					exit(1);
				}
// 获取当前文件的大小，如果当前大小小于已经读取的部分，则表示log没有更新，直接跳过
				fileSize = GetFileSize(hLog, NULL);
				if(hasRead >= fileSize) {
					continue;
				}
				printf("%d %d\n", hasRead, fileSize);
				bufRead = new char[fileSize+1];
// 读取文件
				ReadFile(hLog, bufRead, fileSize, &bytesRead, NULL);
				bufRead[fileSize] = 0;
/*  日志文件格式：
		D:\usr\local\Mysql\MySQL Server 5.5\bin\mysqld, Version: 5.5.28-log (MySQL Community Server (GPL)). started with:
		TCP Port: 3306, Named Pipe: (null)
		Time                 Id Command    Argument
		D:\usr\local\Mysql\MySQL Server 5.5\bin\mysqld, Version: 5.5.28-log (MySQL Community Server (GPL)). started with:
		TCP Port: 3306, Named Pipe: (null)
		Time                 Id Command    Argument
		130813 14:58:11	    1 Connect	root@localhost on 
					1 Query	select @@version_comment limit 1
		130813 14:58:31	    1 Query	show databases
		130813 14:58:45	    1 Query	create database icedemo
		130813 15:00:48	    1 Query	create table test(name varchar(20) not null)
		130813 15:01:31	    1 Query	SELECT DATABASE()
		130813 15:01:32	    1 Init DB	icedemo
		130813 15:01:33	    1 Query	create table test(name varchar(20) not null)
		130813 15:02:42	    1 Query	insert into test(name) values('white')
		130813 15:03:17	    1 Query	select * from test
		130813 20:04:51	    1 Quit	
 * singleLine[0]为数字时，则表明后面会出现SQL语句
*/
				for(i = hasRead, si = 0; i < fileSize+1; i++) {
					if(bufRead[i] == '\n') {						// 读取完一行
						char synSql[1024];
						memset(synSql, 0, sizeof(synSql));
						singleLine[si] = '\0';
						if(singleLine[0] >= '0' && singleLine[0] <= '9' && 'Q' == singleLine[logLen]) {
							int k, sti;
							for(k = logLen+6, sti = 0; singleLine[k] != '\0'; k++, sti++) {
								synSql[sti] = singleLine[k];
							}
							synSql[sti] = ';';
							printf("%s\n", synSql);

							if(
								'i' == synSql[0] || 'I' == synSql[0] ||
								'd' == synSql[0] || 'D' == synSql[0]
							) {
//								printf("sql: %s\n", synSql);
//								transform(synSql, tableName);
								dbOpAgent->synSQL(synSql);
							}

						}
						memset(singleLine, 0, sizeof(singleLine));
						si = 0;
						continue;
					}
					singleLine[si++] = bufRead[i];
				}
				hasRead = fileSize;
				CloseHandle(hLog);
				Sleep(10);
			}
		}
// argc == 3 部分目前已经废除
		else if(3 == argc) {
			if(!strcmp(argv[1], "-s")) {
//				dbOpAgent->synSingleData(argv[2]);
			}
			else {
				fprintf(stderr, "Arguments error\n");
				fprintf(stderr, "Usage: ice_Client [-s sql]\n");
				exit(1);
			}
		}
		else {
			fprintf(stderr, "Arguments error\n");
			fprintf(stderr, "Usage: ice_Client [-s sql]\n");
			exit(1);
		}
    } catch(const Ice::Exception& ex) {
        cerr << ex << endl;
        status = 1;
    } catch(const char* msg) {
        cerr << msg << endl;
        status = 1;
    }
// Ice run time销毁
    if(ic) {
        try {
            ic->destroy();
        } catch (const Ice::Exception& ex) {
            cerr << ex << endl;
            status = 1;
        }
    }
    return status;
}
