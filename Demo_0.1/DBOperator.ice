// **********************************************************************
//
// Author: X_White
//
// synDropTab 情况空数据表
// synAllData 先删除tableName表中所有的信息，然后INSERT 所有数据
// synRowData 同步sqlData（由transmitData传递来的一条数据）
// synSingleData 此处为实现
// transmitData Client 传递到 Server，index为data索引
// **********************************************************************

#pragma once

module dbOperator {
	interface DBOprtr {
		int synDropTab();
		int synAllData();
		int synRowData();
		int synSQL(string s);
		int transmitData(string s, string index);
	};
};