// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

#include <Ice/Ice.h>
#include <Printer.h>
#include <DBoperator.h>

#include <cstdio>
#include <iostream>
#include <cstring>
#include <time.h>
#include <WinSock2.h>
#include <mysql.h>

#pragma comment(lib, "libmysql.lib")

using namespace std;
using namespace Demo;
using namespace dbOperator;

/*
 * Author：X_White
 * 数据库参数格式
*/
typedef struct synData {
	int count;
	char data[20][20];
}synData;
synData sqlData;

MYSQL *conn = NULL;

char dbName[128], tableName[128], hostName[128], sqlName[128], sqlPwd[128];

/*
 * Printer打印服务
*/
class PrinterI : public Printer {
public:
	virtual void printString(const string &, const Ice::Current&);
};

void PrinterI::printString(const string &s, const Ice::Current&) {
	cout << s << endl;
}
//==================================================================================================

/*
 * Author：X_White
 * 数据库操作服务
 * synDropTab 删除tableName表中所有的信息
 * transmitData Client 传递到 Server，index为data索引
 * synRowData 同步sqlData（由transmitData传递来的一条数据）
 * synAllData 先删除tableName表中所有的信息，然后INSERT 所有数据
*/
class DBOprtrI : public DBOprtr {
public:
	virtual int synDropTab(const Ice::Current&);
	virtual int synAllData(const Ice::Current&);
	virtual int synRowData(const Ice::Current&);
	virtual int synSQL(const string &, const Ice::Current&);
	virtual int transmitData(const string &, const string&, const Ice::Current&);
};

int DBOprtrI::synDropTab(const Ice::Current&) {
	char sql[128];
	memset(sql, 0, sizeof(sql));

	strcat_s(sql, "DELETE FROM ");
	strcat_s(sql, tableName);

	if(mysql_query(conn, sql)) {
		fprintf(stderr, "synDropTab failed\n");
		return -1;
	}

	return 0;
}

int DBOprtrI::synAllData(const Ice::Current&) {
	char sql[128];
	memset(sql, 0, sizeof(sql));
	strcat_s(sql, "DELETE * FROM ");
	strcat_s(sql, tableName);

	if(mysql_query(conn, sql)) {
		fprintf(stderr, "mysql_query delete failed\n");
		return -1;
	}

	memset(sql, 0, sizeof(sql));
	strcat_s(sql, "INSERT INTO ");
	strcat_s(sql, tableName);
	strcat_s(sql, " (name, pwd) VALUES (");
	strcat_s(sql, sqlData.data[0]);
	strcat_s(sql, ",");
	strcat_s(sql, sqlData.data[1]);
	strcat_s(sql, ")");

	if(mysql_query(conn, sql)) {
		fprintf(stderr, "mysql_query update failed\n");
		return -1;
	}

	return 0;
}

int DBOprtrI::synRowData(const Ice::Current&) {
	char sql[128];
	memset(sql, 0, sizeof(sql));
	strcat_s(sql, "INSERT INTO ");
	strcat_s(sql, tableName);
	strcat_s(sql, "(name, pwd) VALUES('");
	strcat_s(sql, sqlData.data[0]);
	strcat_s(sql, "','");
	strcat_s(sql, sqlData.data[1]);
	strcat_s(sql, "')");

//	fprintf(stderr, sql);

	if(mysql_query(conn, sql)) {
		fprintf(stderr, "mysql_query insert failed\n");
		return -1;
	}

	return 0;
}

int DBOprtrI::synSQL(const string &sql, const Ice::Current&) {
	char synSQL[128];
	size_t i;
	for(i = 0; sql[i] != '\0'; i++) {
		synSQL[i] = sql[i];
	}
	synSQL[i] = '\0';
	printf("%s\n", synSQL);
	if(mysql_query(conn, synSQL)) {
		fprintf(stderr, "synSQL failed\n");
		return -1;
	}

	return 0;
}

int DBOprtrI::transmitData(const string &data, const string &index, const Ice::Current&) {

	int num = atoi(index.c_str());
	strcpy_s(sqlData.data[num], data.c_str());

	return 0;
}
//==================================================================================================
/*
 * Author：X_White
 * 将des中的数据复制到src
*/
void ice_strcpy(char *src, char *des) {
	size_t i;
	size_t len1 = strlen(src);
	size_t len2 = strlen(des);
	for(i = 0; i < len2; i++) {
		src[i] = des[i];
	}
	src[i] = '\0';
}

/*
 * 写入日志文件
*/
int writeLog() {
	return 0;
}

/*
 * Author：X_White
 * initConf读取配置文件info.conf 并返回数据库dbName、表名
 *
*/

int initConf(char *dbName, char *tableName, char *hostName, char *sqlName, char *sqlPwd) {
	FILE *conffp;
	errno_t err;
	char buf[1024];
	err = freopen_s(&conffp, "../conf/serverInfo.conf", "r", stdin);
	if(err != 0) {
		writeLog();
		exit(1);
	}
	while(gets_s(buf)) {
		if('#' == buf[0]) continue;
		char n1[128], n2[128], equ[128];
		memset(n1, 0, sizeof(n1));
		memset(n2, 0, sizeof(n2));
		sscanf_s(buf, "%s %s %s", n1, sizeof(n1), equ, sizeof(equ) , n2, sizeof(n2));
//		printf("%s %s\n", tmp, num);
		if(!strcmp(n1, "DataBase")) {
			ice_strcpy(dbName, n2);
		}
		else if(!strcmp(n1, "Table")) {
			ice_strcpy(tableName, n2);
		}
		else if(!strcmp(n1, "HostName")) {
			ice_strcpy(hostName, n2);
		}
		else if(!strcmp(n1, "SqlName")) {
			ice_strcpy(sqlName, n2);
		}
		else if(!strcmp(n1, "SqlPwd")) {
			ice_strcpy(sqlPwd, n2);
		}
	}
	return 0;
}

/*
 * Author：X_White
 * Mysql初始化
*/
int sql_init(char *dbName, char *tableName, char *hostName, char *sqlName, char *sqlPwd) {
	conn = mysql_init(NULL);
	if(!conn) {
		fprintf(stderr, "mysql_init failed\n");
		exit(1);
	}
	conn = mysql_real_connect(conn, hostName, sqlName, sqlPwd, dbName, 0, NULL, 0);
	if(!conn) {
		fprintf(stderr, "mysql_real_connect failed\n");
		exit(1);
	}

	return 0;
}

/*
 * Author：X_White
 * main
*/
int main(int argc, char* argv[]) {
	int status = 0;
	Ice::CommunicatorPtr ic;

// 初始化读取配置文件
	initConf(dbName, tableName, hostName, sqlName, sqlPwd);
// 初始化数据库
	sql_init(dbName, tableName, hostName, sqlName, sqlPwd);

	try {
// 初始化Ice run time
		ic = Ice::initialize(argc, argv);
// 创建对象适配器，名称为SimplePrinterAdapter
		Ice::ObjectAdapterPtr adapter =
			ic->createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -h localhost -p 10000");
//		Ice::ObjectAdapterPtr adapter =
//			ic->createObjectAdapterWithEndpoints("DBOperatorAdapter", "default -h localhost -p 10000");
// 为Printer和DBOprtr创建Servant，并加入到适配器中
		Ice::ObjectPtr object = new PrinterI;
		Ice::ObjectPtr objectDB = new DBOprtrI;
		adapter->add(object, ic->stringToIdentity("SimplePrinter"));
		adapter->activate();
		adapter->add(objectDB, ic->stringToIdentity("DBOperator"));
		adapter->activate();
		ic->waitForShutdown();
	} catch(const Ice::Exception& e) {
		cerr << e << endl;
		status = 1;
	} catch(const char* msg) {
		cerr << msg << endl;
		status = 1;
	}

// Ice run time销毁
	if(ic) {
		try {
			ic->destroy();
		} catch(const Ice::Exception& e) {
			cerr << e << endl;
			status = 1;
		}
	}
	mysql_close(conn);

	return status;
}
