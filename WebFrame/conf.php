<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Configuration</title>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js'></script>
<style type="text/css">
@import url("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css");
</style>

<style type="text/css">
#menu4 ul {
	font-family: Verdana, Geneva, sans-serif;
    font-size: 30px;
    letter-spacing: -3px;
    border-top:5px dotted #66b8d8;
    border-bottom:5px dotted #66b8d8;
    margin:20px;
}
#menu4 ul li a{
    color: #66b8d8;
    padding: 10px 5px 10px 10px;
    width: 170px;
}
#menu4 ul li a:hover span.title{
    color:#00adef;
}
#menu4 ul li a span.text{
    font-family: Georgia, serif;
    font-size: 13px;
    color:#c7e7f3;
}
</style>

</head>

<body>

<div id="menu4">
    <ul>
        <li><a href="conf.php">
                <span class="title">Configuration</span>
            </a>
        </li>
        <li><a href="change.php">
                <span class="title">Change Conf</span>
            </a>
        </li>
    </ul>
</div>

<div id="jQueryUIAccordion">
	<h3><a href="#">Client 配置</a></h3>
	<div>
    	<?php
			$clientInfo = fopen("info.conf", "r");			
			while(!feof($clientInfo)) {
				$linex = fgets($clientInfo);
				if('#' == $linex[0] || 2 == strlen($linex)) {
					echo $linex;
					echo "<br />";
					continue;
				}
				$info = (explode(" ", $linex));
				if("ServerIP" == $info[0]) {
					echo "服务器的IP地址：";
				}
				else if("ServerPort" == $info[0]) {
					echo "服务器的端口号：";
				}
				else if("DataBase" == $info[0]) {
					echo "需要同步的数据库名：";	
				}
				else if("Table" == $info[0]) {
					echo "需要同步的数据库表名：";	
				}
				else if("HostName" == $info[0]) {
					echo "需要同步的IP地址：";
				}
				else if("SqlName" == $info[0]) {
					echo "需用同步的数据库用户名：******";
					echo "<br />";
					continue;
				}
				else if("SqlPwd" == $info[0]) {
					echo "需用同步的数据库密码：******";
					echo "<br />";
					continue;
				}
				else if("KEY" == $info[0]) {
					echo "密钥：************";
					echo "<br />";
					continue;
				}
				else {
					echo $info[0];
					echo "：";
				}
				echo $info[2];
				echo "<br />";
			}
		?>
	</div>
	<h3><a href="#">Server 配置</a></h3>
	<div>
		<?php
			$serverInfo = fopen("serverInfo.conf", "r");
			while(!feof($serverInfo)) {
				$linex = fgets($serverInfo);
				$info = (explode(" ", $linex));
				if("DataBase" == $info[0]) {
					echo "目标数据库名：";
				}
				else if("Table" == $info[0]) {
					echo "目标数据库表名：";
				}
				else if("HostName" == $info[0]) {
					echo "目标IP地址：";
				}
				else if("SqlName" == $info[0]) {
					echo "目标数据库用户名：******";
					echo "<br />";
					continue;
				}
				else if("SqlPwd" == $info[0]) {
					echo "目标数据库密码：******";
					echo "<br />";
					continue;
				}
				echo $linex;
				echo "<br />";
			}
        ?>
	</div>
</div>
<script type="text/javascript">
jQuery("#jQueryUIAccordion").accordion({ 
		event: "click",
		collapsible: true,
		autoHeight: true
	});
</script>
</body>
</html>