<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Change Information</title>
<script type='text/javascript' src='Script/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='Script/jquery.password-strength(new).min.js'></script>
</head>

<body>
<script type="text/javascript">
$(document).ready(function() {
	
	var password_settings = { minLength: '12', 
		maxLength: '16', 
		specialLength: '1', 
		upperLength: '1',
		numberLength: '1' 
	 	 
		 }; 
     
	   var myPSPlugin = $("[id$='txtPassword']").password_strength(password_settings);

    });
</script>
<h3>Client: </h3>
<table>
<tr>
	<td>User Name: </td>
    <td><input id="cName" /></td>
</tr>
<tr>
	<td>New Password: </td>
    <td><input id="cPwd" type="password" /></td>
</tr>
<tr>
    <td>Password Confirm:</td>
    <td><input id="cPwdAgain" type="password" /></td>
</tr>
</table>
<button type="button">Submit</button>

<h3>Server: </h3>
<table>
<tr>
	<td>User Name: </td>
    <td><input id="sName" /></td>
</tr>
<tr>
	<td>New Password: </td>
    <td><input id="sPwd" type="password" /></td>
</tr>
<tr>
    <td>Password Confirm:</td>
    <td><input id="sPwdAgain" type="password" /></td>
</tr>
</table>
<button type="button">Submit</button>
</body>
</html>