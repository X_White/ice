8/7/13启动：
Client: 提供两种启动方式
1. /main 全部同步
2. /main -s sql 同步一条SQL
==================================================================


8/14/13更新：
==================================================================
MySQL日志操作注意信息：
1. 打开文件使用CreateFile时，需要以读/写共享锁的方式

本地mysql log配置：
log-bin=D:/var/log/mysql/logbin.log
log-err=D:/var/log/mysql/logerr.log
log=D:/var/log/mysql/log.log
log-slow-queries=D:/var/log/mysql/logslow.log

Server：
1. 修正了synAllData中的sql错误
2. 完善了注释信息

Client：
1. 增加了info.conf对mysql日志的配置信息
2. 增加了initConf中对info.conf的配置读取
3. 实现了单条更新
4. 废除了原来 -s sql 的启动方式


8/21/13更新：
==================================================================
1. 出了Web版本，实现方式为PHP，利用了JQuery框架，仅提供界面以及文件操作的功能，后台尚未完成，界面也有待进一步完善
2. 提供了基于密钥的加密方式，算法有待改进，修改了Client.cpp
3. 设计了Dao数据库操作类 Dao.cpp 以及 Dao.h，但在此次设计最后被废除。该代码已上传


8/25/2013
==================================================================
1. 修复了Client和Server中启动时drop table的BUG
2. 修复了Client中单条更新出错的BUG